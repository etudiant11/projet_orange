class Addition():
    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.s = 0

    def somme(self):

        if isinstance(self.a, (int, float)) and isinstance(self.b, (int, float)):
            self.s = self.a + self.b

            return self.s
        else:
            raise Exception('les 2 valeurs doivent etre des nombres')
