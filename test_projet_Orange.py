# importer le module à tester
import projet_Orange as mp
import unittest

class TestMonProgramme(unittest.TestCase):
    def test_somme(self):


        ob = mp.Addition(4,9)
        resultat=ob.somme()

        self.assertEqual(resultat,13)

    def test_exeption(self):

        ob = mp.Addition('d','b')

        with self.assertRaises(Exception) as ex:
            ob.somme()
        self.assertEqual(str(ex.exception),"les 2 valeurs doivent etre des nombres")

if __name__== "__main__":

    unittest.main()
